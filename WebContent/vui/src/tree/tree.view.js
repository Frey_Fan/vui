/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('TreeView',{
	/**
	 * 构造函数
	 * @constructor
	 */
	init:function(cmp) {
		this._super(cmp);
		
		this.tplId = 'tree_tpl' + cmp.id;
		this.componentName = 'item_' + cmp.id;
		
		this.initVueComponent(cmp);
		if(this.opt('width')) {
			this.$wraper.width(this.opt('width'));
		}
	}
	,initVueComponent:function(cmp) {
		$('body').append(this.getTpl());
		
		var that = this;
		Vue.component(this.componentName, {
			template: '#'+this.tplId
			,props: {
			    model: Object
			}
			,methods:{
				isOpen:function() {
					return cmp.isOpen(this.model);	
				}
				,isLeaf:function() {
					return cmp.isLeaf(this.model);	
				}
				,isClosed:function() {
					return cmp.isClosed(this.model);	
				}
				,renderer:function() {
					return that.renderer(this.model);
				}
				,vClick:function(e) {
					that.vClick(this.model,e);
				}
				,vSelect:function(e) {
					that.vSelect(e,this.model);
				}
				,vToggle:function() {
					that.vToggle(this.model);
				}
			}
		})
	}
	,vToggle:function(item) {
		if(this.cmp.isLeaf(item)) {
			return;
		}
		
		if(this.cmp.isOpen(item)) {
			this.collapse(item);
		}else{
			this.expand(item);
		}
		
	}
	,expand:function(item) {
		var ret = this.cmp.fire('BeforeExpand',{item:item});
		if(ret === false) {
			return;
		}
		item.state = 'open';
		
		this.cmp.fire('Expand',{item:item});
	}
	,collapse:function(item) {
		var ret = this.cmp.fire('BeforeCollapse',{item:item});
		if(ret === false) {
			return;
		}
		item.state = 'closed';
		
		this.cmp.fire('Collapse',{item:item});
	}
	,vClick:function(item,e) {
		this.cmp.fire('Click',{item:item});
		
		this.doClickToggle(item,e);
	}
	,doClickToggle:function(item,e) {
		var clickToggle = this.opt('clickToggle');
		if(clickToggle) {
			var isToggler = $(e.target).hasClass('pui-tree-toggler')				
			if(isToggler) {
				return;
			}
			if(this.cmp.hasChild(item)) {
				this.vToggle(item);
			}
		}
	}
	,vSelect:function(e,item) {
		this.selectedItem = item;
		var ret = this.cmp.fire('BeforeSelect',{item:item});
		if(ret === false) {
			return;
		}
		
		var $target = $(e.target);
		
		this.activeSelect(item,$target);
		
		this.cmp.fire("Select",{item:item});
	}
	,clearSelected:function() {
		var highlightClass = 'ui-state-highlight';
		if(this.beforeSelect) {
			this.beforeSelect.removeClass(highlightClass);
		}
	}
	,activeSelect:function(item,$target) {
		if(this.cmp.hasChild(item) && this.opt('clickToggle')) {
			return;
		}
		this.clearSelected();
		
		$target.addClass('ui-state-highlight');
		
		this.beforeSelect = $target;
	}
	,renderer:function(data) {
		var hander = this.opt('renderer');
		if(hander) {
			return hander(data);
		}
		return data.text;
	}
	,getTemplate:function() {
		return '<div class="pui-tree ui-widget ui-widget-content ui-corner-all">' +
					'<ul class="pui-tree-container">'+
						'<'+this.componentName+' v-for="model in rootData.children" :model="model"></'+this.componentName+'>' +
					'</ul>'+
				'</div>';
	}
	,getTpl:function() {
		var tpl = 
		'<script type="text/x-template" id="'+this.tplId+'">' +
  			'<li class="pui-treenode">' +
			    '<span @click="vClick($event)" class="pui-treenode-content pui-treenode-selectable">' +
					// toggle
				    '<span v-if="isLeaf()" class="pui-treenode-leaf-icon"></span> ' + 
					'<span @click="vToggle()" v-if="isOpen()" class="pui-tree-toggler ui-icon ui-icon-triangle-1-s"></span> ' + 
					'<span @click="vToggle()" v-if="isClosed()" class="pui-tree-toggler ui-icon ui-icon-triangle-1-e"></span> ' + 
				    // icon
				    '<span v-if="isLeaf()" class="pui-treenode-icon ui-icon ui-icon-document"></span> ' + 
					'<span v-if="isOpen()" class="pui-treenode-icon ui-icon ui-icon-folder-open"></span> ' + 
					'<span v-if="isClosed()" class="pui-treenode-icon ui-icon ui-icon-folder-collapsed"></span> ' + 
				    // text
				    '<span ' +
				   	 	'@click="vSelect($event)" ' +
				    	'class="pui-treenode-label ui-corner-all">{{{renderer()}}}</span>' +
				'</span>' +
			    '<ul v-show="isOpen()" class="pui-treenode-children">' +
			      '<'+this.componentName+'' +
			       ' v-for="model in model.children"' +
			       ' :model="model">' +
			      '</'+this.componentName+'>' +
			    '</ul>' +
			'</li>' +
		'</script>';
		
		return tpl;
	}
},VUI.View);

})();