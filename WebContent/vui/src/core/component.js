/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 所有控件的父类
 * @class VUI.Component
 * @extends VUI.Common
 */
VUI.Class('Component',{
	OPTS:{
		/**
		 * @cfg {String} renderId 渲染到目标节点的DOM ID
		 */
		renderId:null
		/**
		 * @cfg {Boolean} autoRender 是否自动渲染
		 */
		,autoRender:true
		/**
		 * @cfg {Boolean} autoHide 是否一开始隐藏
		 */
		,autoHide:false
		/**
		 * @cfg {String} position 控件DIV层的position样式
		 */
		,position:'static'
		/**
		 * @cfg {Number} zindex zindex
		 */
		,zindex:0
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
		
		this.id =  VUI.getId();
		this.controllerName = this.getClassName() + "_Controller_" + this.id;
		this.children = [];
		
		this.initParent();
		this.initChildren();
		
		this.view = this._buildViewInstance();
		
		if(this.opts.autoRender) {
			this.onRender();
			this.render();
		}
	}
	/**
	 * @private
	 */
	,initParent:function() {
		var parent = this.opt('parent');
		if(parent) {
			parent.addChild(this);
		}
	}
	/**
	 * @private
	 */
	,initChildren:function() {
		var children = this.opt('children');
		if(children) {
			children = $.isArray[children] ? children : [children];
			for(var i=0, len=children.length;i<len; i++) {
				this.addChild(children[i]);
			}
		}
	}
	,addChild:function(cmp) {
		this.children.push(cmp);
	}
	/**
	 * 自定义加载
	 * @private
	 */
	,boot:function() {
		if(!this.booted) {
			// 1. 先加载子控件
			var children = this.children;
			for(var i=0, len=children.length;i<len; i++) {
				children[i].boot();
			}
			// 2. 再加载自身
			var appName = this.getAppName()
			
			this.vueData = this.getVueData();
			
			this.controller(this.vueData);
			
			this._vue = new Vue({
				el: '#' + appName,
			  	data:this.vueData
			});
			
			this.booted = true;
			
			this.setNextDo(function(){
				this.afterBoot();
			});
		}
	}
	/**
	 * 下一次执行动作<br>
	 * 如表格数据加载完毕后,勾选某一行:<pre>
	 * grid.setNextDo(function(){
	 *    this.checkRow(1);
	 * });
	 * </pre>
	 * 由于vue的数据绑定是异步的,我们并不知道数据何时绑定完毕<br>
	 * 如果需要在绑定结束后执行某些操作,可以调用这个方法
	 */
	,setNextDo:function(callback) {
		var that = this;
		if(this._vue) {
			this._vue.$nextTick(function () {
				callback.call(that);
			});
		}
	}
	/**
	 * vue对象创建完成后执行的操作
	 */
	,afterBoot:function() {
	}
	/**
	 * @prperty vueData vue的data
	 */
	,vueData:null
	/**
	 * 返回vue的data
	 */
	,getVueData:function() {
		return {
			cmp:this
		};
	}
	,controller:function(data) {
		
	}
	/**
	 * 抽象方法
	 * @abstract
	 */
	,getViewClass:function() {
		alert('类' + this.getClassName() + '必须重写Component.getViewClass()');
	}
	/**
	 * @private
	 */
	,getView:function(){
		return this.view;
	}
	/**
	 * 组件渲染到页面之前触发的事件
	 */
	,onRender:function() {
		
	}
	/**
	 * 组件渲染到页面,如果有dom参数,则渲染到dom中,否则渲染到renderId中
	 * @param {Dom} dom DOM对象
	 */
	,render:function(dom) {
		// 1. 先确保有html
		this.view.render(dom); 
		// 2. 初始化vue
		this.boot();
	}
	/**
	 * @private
	 */
	,getControllerName:function() {
		return this.controllerName;
	}
	/**
	 * @private
	 */
	,getShowName:function() {
		return this.showName;
	}
	/**
	 * 显示控件
	 */
	,show:function() {
		this.view.show();
	}
	/**
	 * 隐藏控件
	 */
	,hide:function() {
		this.view.hide();
	}
	/**
	 * @private
	 */
	,getApp:function() {
		return this.app;
	}
	/**
	 * @private
	 */
	,getAppName:function() {
		return this.getClassName() + "App" + this.id;
	}
	
	,_buildViewInstance:function() {
		var View = this.getViewClass();
		return new View(this)
	}
},VUI.Common);

})();