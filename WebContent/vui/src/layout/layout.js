/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){


var winHeight = $(window).height();
var globalId = 0;



function getTpl(id) {
	var tpl = 
	'<div id="'+id+'">'+
		'<table class="tab-table" cellpadding="0" cellspacing="0">'+
			'<tr>'+
				'<td id="'+id+'_tree" class="dl-second-nav" valign="top"></td>'+
				'<td class="dl-second-body" valign="top">' +
					'<div class="cont-title" id="'+buildContTitleId(id)+'"></div>' +
					'<iframe width="100%" frameborder="0"></iframe>' +
				'</td>'+
			'</tr>'+
		'</table>'+
	'</div>';
	
	return tpl;
}

function buildContTabId(id) {
	return id + "_contTab";
}
function buildContTitleId(id) {
	return id + "_contTitle";
}

/**
 * 页面布局<br>
 * <pre>
var config = 
[
	{
	    id:'permission',
	    text:'系统管理',
	    menu:[{
	        text:'权限管理',
	        children:[
	          {id:'user',text:'用户管理',href:'https://www.baidu.com'}
	          ,{id:'role',text:'角色管理',href:'http://www.csdn.net'}
	          ,{id:'resMgr',text:'资源管理',href:'permission/resMgr.jsp'}
	          ,{id:'group',text:'用户组管理',href:'permission/group.jsp'}
	        ]
	      }
	    ]
	}             
   ,{
       id:'menu', 
       text:'业务管理',
       homePage : 'orderInfo',
       menu:[{
           text:'首页内容',
           children:[
             {id:'orderInfo',text:'订单管理',href:'http://www.soso.com',closeable : false}
           ]
         }
       ]
	}
];
new VUI.Layout({config:config});
 * </pre>
 * @class VUI.Layout
 * @extends VUI.Common
 */
VUI.Class('Layout',{
	OPTS:{
		renderId:'tab'
		,config:null
	}
	/**
	 * 构造函数
	 * @constructor
	 */
	,init:function(opts) {
		this._super(opts);
		this.build(this.opts.config);
	}
	/**
	 * 构建数据
	 */
	,build:function(config) {
		var tabItems = this.getTabItems(config);
		
		new VUI.Tab({
			renderId:this.opt('renderId')
			,items:tabItems
		});
	}
	/**
	 * @private
	 */
	,getTabItems:function(config) {
		var tabItems = [];
	
		for(var i=0, len=config.length;i<len; i++) {
			var cfg = config[i];
			var id = this.createTabContent(cfg);
			var item = {title:cfg.text,contentId:id};
			tabItems.push(item);
		}
		
		return tabItems;
	}
	/**
	 * @private
	 */
	,createTabContent:function(cfg) {
		var tabRenderId = 'tab_' + (cfg.id || globalId++);
		var treeRenderId = tabRenderId + '_tree';
		var tpl = getTpl(tabRenderId);
		var contTitleId = buildContTitleId(tabRenderId);
		
		$('body').append(tpl);
		
		//var contTab = this.createContTab(contTabId);
		
		var $tab = $('#'+tabRenderId);
		var $table = $tab.find('table');
		var $frame = $tab.find('iframe');
		var $title = $('#'+contTitleId).hide();
		
		$table.height(winHeight-64);
		$frame.height(winHeight-104);
		
		this.createTree(cfg,treeRenderId,$frame,$title);
		
		return tabRenderId;
	}
	,createContTab:function(id) {
		var frameId = id + '_frame';
		var tab = new VUI.Tab({
			renderId:id
			,autoHide:true
			,items:[
				{content:'<iframe id="'+frameId+'" width="100%" frameborder="0"></iframe>',checked:true}
			]
		});
		tab.opt('frameId',frameId);
		return tab;
	}
	/**
	 * @private
	 */
	,createTree:function(cfg,treeRenderId,$frame,$title) {
		var that = this;
		new VUI.Tree({
			renderId:treeRenderId
			,data:cfg.menu
			,clickToggle:true
			,onSelect:function(e){
				var item = e.item;
				that.runOptFun('itemFilter',item);
				var openHandler = that.opt('openHandler');
				if(openHandler) {
					openHandler($frame,item,$title);
					return;
				}
				
				if(item.blank) {
					window.open(item.href);				
				}else{
					if(item.href) {
						$title.html(item.text).show();
						$frame.attr('src',item.href);
					}
				}
			}
		});
	}
},VUI.Common);

})();